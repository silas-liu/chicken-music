// 查看网页中数据请求的string parameters中的参数
export const commonParams = {
  g_tk: 1428804945,
  uin: 0,
  format: 'json',
  inCharset: 'utf-8',
  outCharset: 'utf-8',
  notice: 0,
  platform: 'h5',
  needNewCode: 1
}
// 配置jsonp方法中的选项回调
export const options = {
  param: 'jsonpCallback'
}
// 设置请求成功的状态码
// eslint-disable-next-line camelcase
export const ERR_OK = 0